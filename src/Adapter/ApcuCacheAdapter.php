<?php
/**
 * Created by PhpStorm.
 * User: Schneidoa
 * Date: 27.08.17
 * Time: 22:53
 */

namespace Schneidoa\PHPEasyCache\Adapter;




class ApcuCacheAdapter implements CacheAdapterInterface
{
    private $namespace;

    public function __construct(string $namespace) {
        $this->namespace = $namespace;
    }

    public function set(string $key, $value, int $ttl = 0) {
        // TODO: Implement set() method.
        if(apcu_store($this->namespace.$key,$value, $ttl)){
            return $value;
        }else{
            throw new \Exception('Invalid APCU Cache');
        }
    }

    public function get(string $key, $defaultValue = false) {
        $success = false;
        $return = apcu_fetch($this->namespace.$key, $success);
        if ($success){
            return $return;
        }else{
            return $defaultValue;
        }
    }
}