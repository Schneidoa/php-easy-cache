<?php
/**
 * Created by PhpStorm.
 * User: Schneidoa
 * Date: 27.08.17
 * Time: 22:36
 */
namespace Schneidoa\PHPEasyCache\Adapter;
interface CacheAdapterInterface
{

    public function __construct(string $namespace);

    public function set(string $key, $value, int $ttl = 0);
    public function get(string $key, $defaultValue =  false);


}