<?php
/**
 * Created by PhpStorm.
 * User: Schneidoa
 * Date: 27.08.17
 * Time: 22:39
 */
namespace Schneidoa\PHPEasyCache\Adapter;



class NoCacheAdapter implements CacheAdapterInterface
{
    private $cache =  array();

    private $namespace;

    public function __construct(string $namespace) {
        $this->namespace = $namespace;
    }

    public function set(string $key, $value, int $ttl = 0) {
        $this->cache[$key] = $value;
        return $value;
    }

    public function get(string $key, $defaultValue = false) {
        if(isset($this->cache[$key])){
            return $this->cache[$key];
        }else{
            return $defaultValue;
        }

    }

}