<?php
/**
 * Created by PhpStorm.
 * User: Schneidoa
 * Date: 27.08.17
 * Time: 22:42
 */
namespace Schneidoa\PHPEasyCache;
use Schneidoa\PHPEasyCache\Adapter\ApcuCacheAdapter;
use Schneidoa\PHPEasyCache\Adapter\CacheAdapterInterface;
use Schneidoa\PHPEasyCache\Adapter\NoCacheAdapter;

class Cache
{

    /**
     * @var CacheAdapterInterface
     */
    private $cacheAdapter;


    /**
     * Cache constructor.
     * @param string|null $namespace
     * @param string|null $cacheAdapterClass
     * @throws \Exception
     */
    public function __construct(string $namespace = null, string $cacheAdapterClass =  null) {
        if(is_null($namespace)){
            $namespace = $_SERVER['HTTP_HOST'] . '_';
        }else{
            $namespace = $namespace . '_';
        }

        if(is_null($cacheAdapterClass)){
            if(function_exists('apcu_store')){
                $this->cacheAdapter = new ApcuCacheAdapter($namespace);
            }else{
                $this->cacheAdapter = new NoCacheAdapter($namespace);
            }
        }else{
            if(class_exists($cacheAdapterClass)){
                $this->cacheAdapter = new $cacheAdapterClass($namespace);
            }else{
                throw new \Exception('Invalid Cache-Adapter! Class not found! ' . $cacheAdapterClass);
            }

        }


        if(!($this->cacheAdapter instanceof CacheAdapterInterface )){
           throw new \Exception('Invalid Cache-Adapter!' . get_class($this->cacheAdapter));
        }

    }


    /**
     * @param string $key
     * @param $value
     * @param int $ttl
     * @return mixed
     */
    public function set(string $key, $value, $ttl = 0){
        return $this->cacheAdapter->set($key,$value,$ttl);
    }


    /**
     * @param string $key
     * @param bool $defaultValue
     * @return mixed
     */
    public function get(string $key, $defaultValue =  false){
        return $this->cacheAdapter->get($key, $defaultValue);
    }
}