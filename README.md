PHPEasyCache
============

Created by [Daniel Schneider](https://gitlab.com/Schneidoa)

Introduction
------------

PHPEasyCache is a simple PHP Cache Library.  It is designed to be very
simple and easy to extend.


Requirements
------------
* PHP>=7.1
* [APCU](http://php.net/manual/de/book.apcu.php)

DEV

* phpunit/phpunit: "5.4.*"

Installation
------------

### Main Setup

#### With composer

1. Add this project in your composer.json:

    ```json
    "require": {
        "schneidoa/php-easy-cache": "dev-master"
    }

    ```

2. Now tell composer to download PHPEasyCache:

    ```bash
    $ php composer.phar update
    ```

## Usage

```php
 use Schneidoa\PHPEasyCache\Cache;

 $namespace = 'app_production';
 $cacheMe = array(1=> 'Text 1', 2 => 'Text 2');

 $cache =  new Cache($namespace);

 $cache->set('main_info_array', $cacheMe);

 $cachedObject = $cache->get('main_info_array', 'defaultValue');
```